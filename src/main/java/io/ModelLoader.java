package io;

import graphics.Model;
import org.apache.commons.lang3.ArrayUtils;
import org.lwjgl.assimp.AIFace;
import org.lwjgl.assimp.AIMesh;
import org.lwjgl.assimp.AIScene;
import org.lwjgl.assimp.AIVector3D;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

import static org.lwjgl.assimp.Assimp.*;

public class ModelLoader {

    public static Model loadModel(String fileLocation, String extention) {
        ByteBuffer file;
        try {
            file = FileLoader.loadResurce(fileLocation);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
        AIScene scene = aiImportFileFromMemory(file,
                aiProcess_Triangulate |
                        aiProcess_JoinIdenticalVertices |
                        aiProcess_FlipUVs, extention);

        AIMesh mesh = AIMesh.create(scene.mMeshes().get(0));

        ModelData modelData = getModelFromMesh(mesh);
        Model model = new Model(modelData.getVertices(), modelData.getTextures(), modelData.getNormals(),
                modelData.getIndices());
        aiFreeScene(scene);

        return model;
    }

    public static Model loadAllMeshesToModel(String fileLocation, String extention) {
        ByteBuffer file;
        try {
            file = FileLoader.loadResurce(fileLocation);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
        AIScene scene = aiImportFileFromMemory(file,
                aiProcess_Triangulate |
                        aiProcess_JoinIdenticalVertices |
                        aiProcess_FlipUVs, "obj");

        float[] vertices = new float[1];
        float[] textures = new float[1];
        float[] normals = new float[1];
        int[] indices = new int[1];

        List<ModelData> modelData = new ArrayList<>();

        for (int i = 0; i < scene.mNumMeshes(); i++) {
            modelData.add(getModelFromMesh(AIMesh.create(scene.mMeshes().get(i))));
        }

        modelData.forEach(modelData1 -> {
            ArrayUtils.addAll(indices, modelData1.getIndices());
            ArrayUtils.addAll(vertices, modelData1.getVertices());
            ArrayUtils.addAll(normals, modelData1.getNormals());
            ArrayUtils.addAll(textures, modelData1.getTextures());

        });

        return new Model(vertices, textures, normals, indices);

    }

    public static List<Model> loadManyModels(String fileLocation, String extention) {
        ByteBuffer file;
        try {
            file = FileLoader.loadResurce(fileLocation);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
        AIScene scene = aiImportFileFromMemory(file,
                aiProcess_Triangulate |
                        aiProcess_JoinIdenticalVertices |
                        aiProcess_FlipUVs, extention);

        List<Model> models = new ArrayList<>();

        for (int i = 0; i < scene.mNumMeshes(); i++) {
            ModelData modelData = getModelFromMesh(AIMesh.create(scene.mMeshes().get(i)));
            models.add(new Model(modelData.getVertices(), modelData.getTextures(), modelData.getNormals(),
                    modelData.getIndices()));
        }

        aiFreeScene(scene);

        return models;
    }

    private static ModelData getModelFromMesh(AIMesh mesh) {
        float[] dimensions = new float[6];

        float[] vertices = processVertices(mesh, dimensions);

        float[] textureCoordinates = processTextureCoords(mesh);

        float[] normals = processNormals(mesh);

        int[] elements = processElements(mesh);

        return new ModelData(vertices, textureCoordinates, normals, elements);
    }

    private static float[] processNormals(AIMesh mesh) {
        float[] normals = new float[mesh.mNumVertices() * 3];

        int i = 0;
        for (AIVector3D normal : mesh.mNormals()) {
            normals[i++] = normal.x();
            normals[i++] = normal.y();
            normals[i++] = normal.z();
        }

        return normals;
    }

    private static int[] processElements(AIMesh mesh) {
        int[] indices = new int[mesh.mNumFaces() * 3];

        int i = 0;
        for (AIFace face : mesh.mFaces()) {
            indices[i++] = face.mIndices().get(0);
            indices[i++] = face.mIndices().get(1);
            indices[i++] = face.mIndices().get(2);
        }

        return indices;
    }

    private static float[] processTextureCoords(AIMesh mesh) {
        float[] textureCoords = new float[mesh.mNumVertices() * 2];
        if (mesh.mTextureCoords(0) == null) {
            return null;
        }
        int i = 0;
        for (AIVector3D aiVector3D : mesh.mTextureCoords(0)) {
            textureCoords[i++] = aiVector3D.x();
            textureCoords[i++] = aiVector3D.y();
        }
        return textureCoords;
    }

    private static float[] processVertices(AIMesh mesh, float[] dimensions) {
        float[] vertices = new float[mesh.mNumVertices() * 3];

        int i = 0;
        dimensions[0] = Float.MIN_VALUE;
        dimensions[1] = Float.MIN_VALUE;
        dimensions[2] = Float.MIN_VALUE;

        dimensions[3] = Float.MAX_VALUE;
        dimensions[4] = Float.MAX_VALUE;
        dimensions[5] = Float.MAX_VALUE;
        for (AIVector3D vertex : mesh.mVertices()) {
            vertices[i++] = vertex.x();
            vertices[i++] = vertex.y();
            vertices[i++] = vertex.z();

            if (vertex.x() > dimensions[0]) {
                dimensions[0] = vertex.x();
            }
            if (vertex.y() > dimensions[1]) {
                dimensions[1] = vertex.y();
            }
            if (vertex.z() > dimensions[2]) {
                dimensions[2] = vertex.z();
            }

            if (vertex.x() < dimensions[3]) {
                dimensions[3] = vertex.x();
            }
            if (vertex.y() < dimensions[4]) {
                dimensions[4] = vertex.y();
            }
            if (vertex.z() < dimensions[5]) {
                dimensions[5] = vertex.z();
            }
        }

        return vertices;
    }
}
