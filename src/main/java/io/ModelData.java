package io;

public class ModelData {
    private float[] vertices;
    private float[] textures;
    private float[] normals;
    private int[] indices;

    public ModelData(float[] vertices, float[] textures, float[] normals, int[] indices) {
        this.vertices = vertices;
        this.textures = textures;
        this.normals = normals;
        this.indices = indices;
    }

    public float[] getVertices() {
        return vertices;
    }

    public float[] getTextures() {
        return textures;
    }

    public float[] getNormals() {
        return normals;
    }

    public int[] getIndices() {
        return indices;
    }

}
