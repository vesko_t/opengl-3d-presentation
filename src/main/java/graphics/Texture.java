package graphics;

import io.FileLoader;
import org.lwjgl.system.MemoryStack;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL13.GL_TEXTURE0;
import static org.lwjgl.opengl.GL13.glActiveTexture;
import static org.lwjgl.stb.STBImage.*;

/**
 * Created by Veselin Trichkov on 22.1.2018 г..
 */
public class Texture {

    private int textureId;

    private int width;

    private int height;

    private ByteBuffer buffer;

    public Texture(int textureId, int width, int height) {
        this.textureId = textureId;
        this.width = width;
        this.height = height;
    }

    public Texture(String fileName) {

        ByteBuffer buffer = null;

        try (MemoryStack stack = MemoryStack.stackPush()) {
            /* Prepare image buffers */
            IntBuffer w = stack.mallocInt(1);
            IntBuffer h = stack.mallocInt(1);
            IntBuffer comp = stack.mallocInt(1);

            /* Load image */
            stbi_set_flip_vertically_on_load(false);
            buffer = stbi_load_from_memory(FileLoader.loadResurce(fileName), w, h, comp, 4);
            if (buffer == null) {
                throw new RuntimeException("Failed to load a texture file!"
                        + System.lineSeparator() + stbi_failure_reason());
            }
            /* Get width and height of image */
            width = w.get();
            height = h.get();
            textureId = glGenTextures();

            w.clear();
            h.clear();
            comp.clear();
            w = null;
            h = null;
            comp = null;

            glBindTexture(GL_TEXTURE_2D, textureId);

            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);


            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, buffer);


        } catch (IOException e) {
            e.printStackTrace();
        }

        this.buffer = buffer;
        buffer = null;
    }

    public ByteBuffer getBuffer() {
        return buffer;
    }

    /**
     * binds texture to texture sampler
     *
     * @param sampler the id ot the sampler
     */
    public void bind(int sampler) {
        glActiveTexture(GL_TEXTURE0 + sampler);
        glBindTexture(GL_TEXTURE_2D, textureId);
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

}

