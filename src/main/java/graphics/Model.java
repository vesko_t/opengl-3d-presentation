package graphics;

import org.lwjgl.BufferUtils;
import org.lwjgl.system.MemoryStack;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL15.*;
import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL30.glBindVertexArray;
import static org.lwjgl.opengl.GL30.glGenVertexArrays;
import static org.lwjgl.system.MemoryStack.stackPush;

/**
 * Created by Veso on 22.1.2018 г..
 */
public class Model {
    private int vaoId, count;

    public Model(float[] vertices, float[] texCoords, float[] normals, int[] indices) {
        IntBuffer indexBuffer = BufferUtils.createIntBuffer(indices.length);
        indexBuffer.put(indices).flip();

        FloatBuffer vertexBuffer = BufferUtils.createFloatBuffer(vertices.length);
        vertexBuffer.put(vertices).flip();

        FloatBuffer textureBuffer = null;
        if (texCoords != null) {
            textureBuffer = BufferUtils.createFloatBuffer(texCoords.length);
            textureBuffer.put(texCoords).flip();
        }

        FloatBuffer normalBuffer = null;
        if (normals != null) {
            normalBuffer = BufferUtils.createFloatBuffer(normals.length);
            normalBuffer.put(normals).flip();
        }
        fillBuffers(vertexBuffer, textureBuffer, normalBuffer, indexBuffer, indices.length);
    }

    private void fillBuffers(FloatBuffer vertices, FloatBuffer texCoords, FloatBuffer normals, IntBuffer indices, int numIndices) {
        count = numIndices;

        vaoId = glGenVertexArrays();

        glBindVertexArray(vaoId);
        try (MemoryStack stack = stackPush()) {
            int indicesId;
            int verticesId;
            int textureId;
            int normalId;

            indicesId = glGenBuffers();
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indicesId);

            glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices, GL_STATIC_DRAW);

            verticesId = glGenBuffers();
            glBindBuffer(GL_ARRAY_BUFFER, verticesId);
            glBufferData(GL_ARRAY_BUFFER, vertices, GL_STATIC_DRAW);
            glVertexAttribPointer(0, 3, GL_FLOAT, false, 0, 0);

            if (texCoords != null) {
                textureId = glGenBuffers();
                glBindBuffer(GL_ARRAY_BUFFER, textureId);
                glBufferData(GL_ARRAY_BUFFER, texCoords, GL_STATIC_DRAW);
                glVertexAttribPointer(1, 2, GL_FLOAT, false, 0, 0);
            }

            if (normals != null) {
                normalId = glGenBuffers();
                glBindBuffer(GL_ARRAY_BUFFER, normalId);
                glBufferData(GL_ARRAY_BUFFER, normals, GL_STATIC_DRAW);
                glVertexAttribPointer(2, 3, GL_FLOAT, false, 0, 0);
            }
        }
        glBindVertexArray(0);

    }

    public void render() {

        glBindVertexArray(vaoId);

        glEnableVertexAttribArray(0);
        glEnableVertexAttribArray(1);
        glEnableVertexAttribArray(2);

        glDrawElements(GL_TRIANGLES, count, GL_UNSIGNED_INT, 0);

        glDisableVertexAttribArray(0);
        glDisableVertexAttribArray(1);
        glDisableVertexAttribArray(2);

        glBindVertexArray(0);
    }
}
