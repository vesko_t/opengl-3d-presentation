import graphics.Model;
import graphics.Texture;
import io.Input;
import io.ModelLoader;
import io.Window;
import org.joml.Vector3f;
import org.lwjgl.glfw.GLFW;
import rendering.Camera;
import rendering.Renderable;
import rendering.Shader;

import java.util.ArrayList;
import java.util.List;

import static org.lwjgl.opengl.GL11.glClearColor;

public class Main {

    public Main() {
        Window window = new Window(1280, 800, "Presentation", true);
        new Camera(Window.getWidth(), Window.getHeight());

        Model dragonModel = ModelLoader.loadModel("/objs/dragon.obj", "obj");

        Texture tile2 = new Texture("/textures/tile2.png");

        List<Texture> slides = new ArrayList<>();
        for (int i = 0; i < 6; i++) {
            slides.add(new Texture("/textures/slide" + i + ".png"));
        }

        Model plane = new Model(new float[]{
                -50, -50, 0,
                -50, 50, 0,
                50, 50, 0,
                50, -50, 0
        }, new float[]{
                0, 1,
                0, 0,
                1, 0,
                1, 1

        }, new float[]{
                0, 0, 1
        }, new int[]{
                0, 1, 2,
                0, 2, 3
        });

        Model screenModel = new Model(new float[]{
                -80, -45, 0,
                -80, 45, 0,
                80, 45, 0,
                80, -45, 0
        }, new float[]{
                0, 1,
                0, 0,
                1, 0,
                1, 1

        }, new float[]{
                0, 0, 1
        }, new int[]{
                0, 1, 2,
                0, 2, 3
        });

        Shader shader = new Shader("test");

        Renderable floor = new Renderable(plane, tile2, new Vector3f());
        floor.scale(new Vector3f(2f));
        floor.rotateX((float) Math.toRadians(-90));

        Renderable dragon = new Renderable(dragonModel, new Vector3f(18, 0, 0));
        dragon.scale(new Vector3f(4));

        Renderable screen = new Renderable(screenModel, slides.get(0), new Vector3f(-25, 45 + 10, 0));
        screen.scale(new Vector3f(0.5f));
        screen.rotateY((float) Math.toRadians(45));

        Vector3f lightPosition = new Vector3f(25);
        Vector3f lightColor = new Vector3f(1);

        int slide = 0;

        float velocityConst = 50;

        int FPS = 60;

        double updateTicks = 1000000000 / (double) FPS;

        double deltaTicks = 0;

        double lastTime = System.nanoTime();

        int frames = 0;

        long timer = System.currentTimeMillis();

        int lastFrames = 60;

        int totalFrames = 0;

        Camera.setPosition(new Vector3f(-7.48f, -screen.getPosition().y / 2.0f, -22.6f));
        Camera.getRotation().y = (float) Math.toRadians(-45);

        while (!window.shouldClose()) {
            long now = System.nanoTime();
            deltaTicks += (now - lastTime) / updateTicks;
            lastTime = now;
            if (deltaTicks >= 1) {
                window.prepare();

                glClearColor(0.25f, 0, 0.5f, 1);

                float delta = (float) (1.0 / (float) lastFrames);

                Vector3f cameraVelocity = new Vector3f();

                if (Input.isKeyDown(GLFW.GLFW_KEY_W)) {
                    cameraVelocity.z = delta * velocityConst;
                } else if (Input.isKeyDown(GLFW.GLFW_KEY_S)) {
                    cameraVelocity.z = delta * -velocityConst;
                } else {
                    cameraVelocity.z = 0;
                }

                if (Input.isKeyDown(GLFW.GLFW_KEY_A)) {
                    cameraVelocity.x = delta * velocityConst;
                } else if (Input.isKeyDown(GLFW.GLFW_KEY_D)) {
                    cameraVelocity.x = delta * -velocityConst;
                } else {
                    cameraVelocity.x = 0;
                }

                if (Input.isKeyDown(GLFW.GLFW_KEY_LEFT_CONTROL)) {
                    cameraVelocity.y = delta * velocityConst;
                } else if (Input.isKeyDown(GLFW.GLFW_KEY_LEFT_SHIFT)) {
                    cameraVelocity.y = delta * -velocityConst;
                } else {
                    cameraVelocity.y = 0;
                }

                if (Input.isKeyDown(GLFW.GLFW_KEY_E)) {
                    Camera.getRotation().y += -delta;
                } else if (Input.isKeyDown(GLFW.GLFW_KEY_Q)) {
                    Camera.getRotation().y += delta;
                }

                if (Input.isKeyDown(GLFW.GLFW_KEY_ESCAPE)) {
                    Window.close();
                }

                if (Input.isKeyPressed(GLFW.GLFW_KEY_SPACE)) {
                    slide++;
                }

                if (slide == 0) {
                    screen.setTexture(slides.get(0));
                } else if (slide == 1) {
                    screen.setTexture(slides.get(1));
                } else if (slide == 3) {
                    screen.setTexture(slides.get(2));
                } else if (slide == 4) {
                    screen.setTexture(slides.get(3));
                } else if (slide == 2) {
                    if (Camera.getRotation().y < 0) {
                        Camera.getRotation().y += 0.2 * delta;
                    }
                    if (Camera.getPosition().z > -80) {
                        cameraVelocity.z = -50.0f / 120.0f;
                    }
                    if (Camera.getPosition().x > -20) {
                        cameraVelocity.x = -10.0f / 120.0f;
                    }
                } else if (slide == 5) {
                    screen.setTexture(slides.get(4));
                } else if (slide == 6) {
                    screen.setTexture(slides.get(5));
                }

                Camera.getPosition().add(cameraVelocity);

                //render stuff
                shader.bind();
                shader.setUniform("sampler", 0);
                shader.setUniform("projection", Camera.getProjection());
                shader.setUniform("lightPosition", lightPosition);
                shader.setUniform("lightColor", lightColor);
                shader.setUniform("reflectivity", 2.0f);
                shader.setUniform("inverseProjection", Camera.getProjection().invert());

                //dragon
                shader.setUniform("hasTexture", 0);
                shader.setUniform("transform", dragon.getTransformationMatrix());
                shader.setUniform("view", dragon.getViewMatrix());
                shader.setUniform("color", new Vector3f(0, 0.5f, 0));
                dragon.rotateY(0.5f * delta);
                dragon.render();

                //floor
                shader.setUniform("hasTexture", 1);
                shader.setUniform("transform", floor.getTransformationMatrix());
                shader.setUniform("view", floor.getViewMatrix());
                floor.render();

                //screen
                shader.setUniform("transform", screen.getTransformationMatrix());
                shader.setUniform("view", screen.getViewMatrix());
                shader.setUniform("reflectivity", 0.2f);
                screen.render();

                frames++;
                totalFrames++;
                deltaTicks--;
                window.swapBuffers();
                Input.update();
            }
            if (System.currentTimeMillis() - timer > 1000) {
                timer += 1000;

                System.out.println("FPS : " + frames);

                //lastFrames = frames;

                frames = 0;
            }
        }
        window.terminate();
    }

    public static void main(String[] args) {
        new Main();
    }

}
