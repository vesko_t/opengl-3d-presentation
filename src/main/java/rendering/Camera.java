package rendering;

import org.joml.Matrix4f;
import org.joml.Vector2f;
import org.joml.Vector3f;

/**
 * Created by Veso on 22.1.2018 г..
 */
public class Camera {
    private static Vector3f position;
    private static Matrix4f projection;
    private static int width;
    private static int height;
    private static Vector3f velocity;
    private static Vector3f rotation = new Vector3f();

    public Camera() {
        this(1280, 720);
    }

    public Camera(int width, int height) {
        this.width = width;
        this.height = height;
        position = new Vector3f(0);
        projection = new Matrix4f().perspective((float) Math.toRadians(70), 16.0f/9.0f, 0.01f, 1000f);
        velocity = new Vector3f();
    }

    public static Vector3f getPosition() {
        return position;
    }

    public static void setPosition(Vector3f position) {
        Camera.position = position;
    }

    public static Matrix4f getProjection() {
        Matrix4f transformed = new Matrix4f(projection);
        transformed.rotateXYZ(rotation.x, rotation.y, rotation.z);
        transformed.translate(position);
        return transformed;
    }

    public static void addPosition(Vector3f vector3f) {

    }

    public static int getWidth() {
        return width;
    }

    public static int getHeight() {
        return height;
    }

    public static Vector3f getVelocity() {
        return velocity;
    }

    public static void setVelocity(Vector3f velocity) {
        Camera.velocity = velocity;
    }

    public static void rotateX(float angle) {
        projection.rotateX(angle);
    }

    public static void rotateY(float angle) {
        projection.rotateY(angle);
    }

    public static void rotateZ(float angle) {
        projection.rotateZ(angle);
    }

    public static Vector2f getPositionCamera(Vector2f vector2f) {
        return new Vector2f(vector2f.x - (-position.x), vector2f.y - (-position.y));
    }

    public static Vector2f getPositionScreen(Vector2f vector2f) {
        Vector2f cameraPosition = getPositionCamera(vector2f);

        return new Vector2f(cameraPosition.x + width / 2.0f, (cameraPosition.y - height / 2.0f) * -1);
    }

    public static Vector2f getGlPossition(Vector2f vector2f) {

        Vector2f cameraPos = getPositionCamera(vector2f);

        return new Vector2f(cameraPos.x / width * 2, cameraPos.y / height * 2);
    }

    public void setScale(Vector3f scale) {
        projection.scale(scale);
    }

    public static Vector3f getRotation() {
        return rotation;
    }
}
