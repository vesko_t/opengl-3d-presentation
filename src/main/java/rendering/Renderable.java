package rendering;

import graphics.Model;
import graphics.Texture;
import org.joml.Matrix4f;
import org.joml.Vector3f;

public class Renderable {

    private Model model;
    private Texture texture;
    private Matrix4f transformationMatrix = new Matrix4f();
    private Matrix4f viewMatrix = new Matrix4f();
    private Vector3f position;

    public Renderable(Model model, Vector3f position) {
        this.model = model;
        this.position = position;
        transformationMatrix.setTranslation(position);
    }

    public Renderable(Model model, Texture texture, Vector3f position) {
        this.model = model;
        this.texture = texture;
        this.position = position;
        transformationMatrix.setTranslation(position);
    }

    public void render() {
        if (texture != null){
            texture.bind(0);
        }
        model.render();
    }

    public Model getModel() {
        return model;
    }

    public void setModel(Model model) {
        this.model = model;
    }

    public Texture getTexture() {
        return texture;
    }

    public void setTexture(Texture texture) {
        this.texture = texture;
    }

    public Matrix4f getTransformationMatrix() {
        return transformationMatrix;
    }

    public void setTransformationMatrix(Matrix4f transformationMatrix) {
        this.transformationMatrix = transformationMatrix;
    }

    public Vector3f getPosition() {
        return position;
    }

    public void setPosition(Vector3f position) {
        transformationMatrix.setTranslation(position);
        this.position = position;
    }

    public Matrix4f getViewMatrix() {
        return viewMatrix;
    }

    public void setViewMatrix(Matrix4f viewMatrix) {
        this.viewMatrix = viewMatrix;
    }

    public void scale(Vector3f scale) {
        viewMatrix.scale(scale);
    }

    public void rotateX(float angle) {
        transformationMatrix.rotateX(angle);
    }

    public void rotateY(float angle) {
        transformationMatrix.rotateY(angle);
    }

    public void rotateZ(float angle) {
        transformationMatrix.rotateZ(angle);
    }

}
