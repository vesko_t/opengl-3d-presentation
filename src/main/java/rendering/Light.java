package rendering;

import org.joml.Matrix4f;
import org.joml.Vector3f;

public class Light {

    private Vector3f position;
    private Vector3f color;
    private Matrix4f projectionMatrix;

    public Light(Vector3f position, Vector3f color) {
        this.position = position;
        this.color = color;
        projectionMatrix = new Matrix4f().perspective(70, 16.0f/9.0f,0.01f, 100);
        //projectionMatrix = new Matrix4f().ortho(1280, -1280, -720, 730, 0.01f, 25);
        projectionMatrix.setTranslation(position);
    }

    public Vector3f getPosition() {
        return position;
    }

    public void rotateX(float angle) {
        projectionMatrix.rotateX(angle);
    }

    public void rotateY(float angle) {
        projectionMatrix.rotateY(angle);
    }

    public void rotateZ(float angle) {
        projectionMatrix.rotateZ(angle);
    }

    public void setPosition(Vector3f position) {
        projectionMatrix.setTranslation(position);
        this.position = position;
    }

    public Vector3f getColor() {
        return color;
    }

    public void setColor(Vector3f color) {
        this.color = color;
    }

    public Matrix4f getProjectionMatrix() {
        return projectionMatrix;
    }

    public void setProjectionMatrix(Matrix4f projectionMatrix) {
        this.projectionMatrix = projectionMatrix;
    }
}
