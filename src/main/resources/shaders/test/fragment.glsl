#version 120

uniform sampler2D sampler;
uniform int hasTexture;
uniform vec3 color;
uniform vec3 lightColor;
uniform float reflectivity;

varying vec2 texCoords;
varying vec3 normals;
varying vec3 lightVector;
varying vec3 toCameraVector;

void main() {
    vec3 unitNormal = normalize(normals);
    vec3 unitToLight = normalize(lightVector);

    float difuzeDot = dot(unitToLight, unitNormal);
    float brightness = max(difuzeDot, 0.1);
    vec3 difuze = brightness * lightColor;

    vec3 unitToCamera = normalize(toCameraVector);
    vec3 lightDirection = -unitToLight;
    vec3 reflectedLight = reflect(lightDirection, unitNormal);

    float specularFactor = dot(reflectedLight, unitToCamera);
    specularFactor = max(specularFactor, 0.0);
    float dampedFactor = pow(specularFactor, 10);
    vec3 finalSpecular = dampedFactor * reflectivity * lightColor;


    if(hasTexture == 1){
        gl_FragColor = vec4(difuze,1) * texture2D(sampler, texCoords) + vec4(finalSpecular, 1.0);
    }else {
        gl_FragColor =vec4(difuze,1) * vec4(color, 1.0) + vec4(finalSpecular, 1.0);
    }

}
