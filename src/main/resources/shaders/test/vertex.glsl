#version 120

attribute vec3 vertices;
attribute vec2 textures;
attribute vec3 normal;

uniform mat4 projection;
uniform mat4 transform;
uniform mat4 view;
uniform mat4 inverseProjection;
uniform vec3 lightPosition;

varying vec2 texCoords;
varying vec3 normals;
varying vec3 lightVector;
varying vec3 toCameraVector;
void main() {

    vec4 worldPosition = transform * vec4(vertices, 1.0);
    gl_Position = projection * view * worldPosition;
    texCoords = textures;
    normals = (transform * vec4(normal, 0.0)).xyz;
    lightVector = lightPosition - worldPosition.xyz ;
    toCameraVector = (inverseProjection * vec4(0.0,0.0,0.0,1)).xyz - worldPosition.xyz;
}
